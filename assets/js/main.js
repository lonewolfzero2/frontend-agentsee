function mainload()
{
	$(":input[type='text'], :input[type='password'], textarea").wijtextbox();
	$("select").selectmenu();
	$(":input[type='radio']").wijradio();
	$(":input[type='checkbox']").wijcheckbox();
	$(".button, :input[type='submit']").button();
}

function autoagency(id)
{
	$(id).autocomplete({
		source: function( request, response )
		{
			$.ajax({
				url: "http://api.prop.net.au/agency/autocomplete",
				dataType: "jsonp",
				data: {
					agency: request.term
				},
				success: function(data)
				{
					response($.map(data, function(item) {
						return {
							label: item.name,
							value: item.name,
							id: item.id,
							address: item.address,
							suburb: item.suburb
						};
					}));
				}
			});
		},
		minLength: 2,
		select: function( event, ui ) {
			$.ajax({
				url: "http://api.prop.net.au/agency/get",
				dataType: "jsonp",
				data: {
					id: ui.item.id
				},
				success: function(data)
				{
					$("#form_office_id").val(data.id);
					$("#form_agency_address").val(data.address);
					$("#form_agency_phone").val(data.phone);
					$("#form_agency_fax").val(data.fax);
					$("#form_re_id").val(data.re_id);
				}
			});
			return ui.item.label;
		},
		open: function() {
			$( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
		},
		close: function() {
			$( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
		}
	}).data("autocomplete")._renderItem = function (ul, item) {
		return $("<li></li>").data("item.autocomplete", item).append("<a>" + item.label + "<br><span class='small'>" + (item.address == "" ? "" : item.address + ", ") + item.suburb + "</span></a>").appendTo(ul);
	};
}

function autoagency2(id)
{
	$(id).autocomplete({
		source: function( request, response )
		{
			$.ajax({
				url: "/ajax/agency",
				dataType: "json",
				data: {
					q: request.term
				},
				success: function(data)
				{
					response($.map(data, function(item) {
						return {
							label: item.name,
							value: item.name,
							id: item.id,
							address: item.address,
						};
					}));
				}
			});
		},
		minLength: 2,
		select: function( event, ui ) {
			$("#form_office_id").val(ui.item.id);
			return ui.item.label;
		},
		open: function() {
			$( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
		},
		close: function() {
			$( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
		}
	}).data("autocomplete")._renderItem = function (ul, item) {
		return $("<li></li>").data("item.autocomplete", item).append("<a>" + item.label + (item.address == "" ? "" : "<br><span class='small'>" + item.address + "</span>") + "</a>").appendTo(ul);
	};
}

function generatePassword(id)
{
	var allowed = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0"];
	var pass = "";

	for (var i = 0; i < 16; i++)
	{
		pass = pass + allowed[Math.floor(Math.random() * allowed.length)];
	}

	$("#" + id).val(pass);

	return false;
}
