<?php

class FormGen
{
	private $multiforms = array();
	private $current_form = 0;

	function __construct()
	{
		$CI = get_instance();
		$this->action = $CI->uri->uri_string();
		$CI->load->helper('url');
	}
	
	function swap_form($i = 0)
	{
		$this->current_form = $i;
		
		if (empty($this->multiforms[$this->current_form]))
		{
			$this->multiforms[$this->current_form] = array(
				'errors' => array(),
				'data' => array(),
				'multi_part' => false,
				'action' => '',
				'inline_errors' => true,
				'groups' => array(),
				'group' => 0,
				);
		}
	}
	
	function &__get($name)
	{
		return $this->multiforms[$this->current_form][$name];
	}
	
	function __set($name, $value)
	{
		$this->multiforms[$this->current_form][$name] = $value;
	}

	function clear()
	{
		$this->groups = array();
		$this->group = 0;
		$this->errors = array();
		$this->data = array();
		$this->multi_part = 0;
	}

	function add_group($name = '')
	{
		$this->group++;
		$this->groups[$this->group] = array(
			'legend' => $name,
			'fields' => array(),
			);
	}

	function add_field($field = array())
	{
		if ($this->group == 0)
			$this->add_group();

		if ($field['type'] == 'file')
			$this->multi_part = true;

		$field['group'] = $this->group;

		if (!isset($field['id']))
			$field['id'] = 'form_'.$field['name'];

		$this->groups[$this->group]['fields'][] = $field;
	}

	function show_form($form)
	{
		$this->clear();
		
		foreach ($form as $f)
		{
			if ($f['type'] == 'group')
				$this->add_group($f['name']);
			else
				$this->add_field($f);
		}
	}

	function submitted($multiform_id = -1)
	{
		if ($multiform_id > -1)
			$this->swap_form($multiform_id);

		$CI = get_instance();
		return $CI->input->server('REQUEST_METHOD') == 'POST' && $CI->input->post('frmgen') == $this->current_form;
	}

	function validate($multiform_id = -1)
	{
		if ($multiform_id > -1)
			$this->swap_form($multiform_id);
		
		$valid = true;
		$this->errors = array();
		$CI = get_instance();
		
		foreach ($this->groups as $group)
		{
			foreach ($group['fields'] as $field)
			{
				if (in_array($field['type'], array('html')))
					continue;

				$input = $CI->input->post($field['name']);
				if (isset($field['validation']))
				{
					foreach ($field['validation'] as $test => $param)
					{
						if (is_numeric($test))
							$test = $param;
						switch ($test)
						{
							case 'required':
								if (empty($input))
								{
									$valid = false;
									$this->errors[$field['name']] = $field['label'].' is required';
								}
								break;
							case 'greater_than_zero':
								if ($input == 0)
								{
									$valid = false;
									$this->errors[$field['name']] = $field['label'].' is required';
								}
								break;
							case 'trim':
								$input = trim($input);
								break;
							case 'force_numeric':
								preg_replace('|[^\d]|', '', $input);
								break;
							case 'str_remove':
								$input = str_replace($param, '', $input);
								break;
							case 'int':
								if (!is_numeric($input))
								{
									$valid = false;
									$this->errors[$field['name']] = $field['label'].' is not a whole number.';
								}
								break;
							case 'min_length':
								if (strlen($input) < $param)
								{
									$valid = false;
									$this->errors[$field['name']] = $field['label'].' is too short. The minimum length is '.$param;
								}
								break;
							case 'exact_length':
								if (strlen($input) != $param)
								{
									$valid = false;
									$this->errors[$field['name']] = $field['label'].' must be exactly '.$param.' characters long';
								}
								break;
							case 'max_length':
								if (strlen($input) > $param)
								{
									$valid = false;
									$this->errors[$field['name']] = $field['label'].' is too long. The maximum length is '.$param;
								}
								break;
							case 'same_as':
								$input2 = isset($this->data[$param]) ? $this->data[$param] : $CI->input->post($parma);
								if ($input != $input2)
								{
									$f = array();
									foreach ($this->groups as $g2)
									{
										foreach ($g2['fields'] as $fld)
										{
											if ($fld['name'] == $param)
											{
												$f = $fld;
												break 2;
											}
										}
									}
									if (empty($f))
										break;
									
									$valid = false;
									$this->errors[$field['name']] = $f['label'].' and '.$field['label'].' do not match.';
								}
								break;
							case 'valid_mobile':
								preg_replace('|[^\d]|', '', $input);

								if (strlen($input) != 10 || substr($input, 0, 2) != '04')
								{
									$valid = false;
									$this->errors[$field['name']] = 'Please enter a valid Australian mobile number.';
								}
								break;
							case 'valid_email':
								if (!empty($input) && !filter_var($input, FILTER_VALIDATE_EMAIL))
								{
									$valid = false;
									$this->errors[$field['name']] = $field['label'].' is not a valid email address.';
								}
								break;
							case 'valid_website':
								if (!filter_var($input, FILTER_VALIDATE_URL))
								{
									$valid = false;
									$this->errors[$field['name']] = $field['label'].' is not a valid website. Please include the http://';
								}
								break;
							case 'function':
								$return = call_user_func_array($param['func'], array_merge(array($input), $param['param']));
								
								if (!$return['valid'])
								{
									$valid = false;
									if (!empty($return['error']))
									{
										$this->errors[$field['name']] = $return['error'];
									}
								}
								
								if (isset($return['input']))
									$input = $return['input'];
								break;
						}
					}
				}

				if ($field['type'] == 'checkbox')
				{
					$this->data[$field['name']] = empty($input) ? 0 : 1;
				} else {
					$this->data[$field['name']] = $input;
				}
			}
		}

		return $valid;
	}

	function repopulate($array = array())
	{
		$data = empty($array) ? $this->data : $array;
		foreach ($this->groups as &$group)
		{
			foreach ($group['fields'] as &$field)
			{
				if (isset($data[$field['name']]))
				{
					if ($field['type'] == 'checkbox')
						$field['value'] = !empty($data[$field['name']]) ? $field['preset'] : -1;
					else
						$field['value'] = $data[$field['name']];
				}
			}
		}
	}

	function render($multiform_id = -1)
	{
		if ($multiform_id > -1)
			$this->swap_form($multiform_id);

		$output = '';
		$append_last = '';

		$output .= '<form method="post" action="'.site_url($this->action).'" '.($this->multi_part ?' enctype="multipart/form-data"' : '').'>';
		$output .= '<input type="hidden" name="frmgen" value="'.$this->current_form.'">';

		if (($this->errors) && !$this->inline_errors)
		{
			$output .= '<div class="ui-widget"><div class="ui-state-error ui-corner-all rmbtm" style="padding: 0 .7em;"><p><span class="ui-icon ui-icon-alert" style="float:left;margin-right:.3em;"></span><strong>Error:</strong> There was a problem validating the data you submitted.</p><ul><li>'.implode('</li><li>', $this->errors).'</li></ul></div></div>';
		}
		
		if (!empty($this->errors['controller']) && ($this->inline_errors))
			$output .= '<div class="inline-error">'.$this->errors['controller'].'</div>';
		
		foreach ($this->groups as $group)
		{
			$output .= '<fieldset>';
			if (!empty($group['legend']))
				$output .= '<legend>'.$group['legend'].'</legend>';
			$output .= '<ol>';

			foreach ($group['fields'] as $field)
			{
				if (empty($field))
					continue;

				if ($field['type'] == 'hidden')
				{
					$append_last .= '<input type="'.$field['type'].'" name="'.$field['name'].'" id="'.$field['id'].'" value="'.$field['value'].'">';
					continue;
				}
				
				$output .= '<li'.(isset($field['list_style']) ? ' class="'.$field['list_style'].'"' : '').'>';
				if (isset($field['list_style']))
					unset($field['list_style']);

				if (!empty($field['label']) && empty($field['append_label']))
					$output .= '<label for="'.$field['id'].'"'.(empty($field['label_class']) ? '' : ' class="'.$field['label_class'].'"').'>'.$field['label'].'</label>';

				$standard_input = false;
				$disable_value = false;
				$extra = '';

				switch ($field['type'])
				{
					case 'html':
						$output .= $field['value'];
						break;
					case 'textarea':
						$output .= '<textarea name="'.$field['name'].'" id="'.$field['id'].'" rows="'.$field['rows'].'" cols="'.$field['cols'].'"'.(!empty($field['class']) ? ' class="'.$field['class'].'"' : '').'>'.htmlspecialchars($field['value'], ENT_QUOTES).'</textarea>';
						break;
					case 'select':
						$output .= '<select name="'.$field['name'].'" id="'.$field['id'].'">';
						if (isset($field['values'][0]) && $field['values'][0] == '--load')
						{
							$CI = get_instance();
							$model = $field['values'][1];
							$func = $field['values'][2];
							$args = array();
							if (isset($field['values'][3]))
								$args = array_slice($field['values'], 3);
							$CI->load->model($model);

							$field['values'] = call_user_func_array(array($CI->$model, $func), $args);

							if (isset($field['prepend_values']))
								$field['values'] = $field['prepend_values'] + $field['values'];
						}
						
						foreach ($field['values'] as $key => $val)
						{
							$output .= '<option value="'.$key.'"'.($field['value'] == $key ? ' selected="selected"' : '').'>'.$val.'</option>';
						}
						$output .= '</select>';
						break;
					case 'radio':
						$standard_input = true;
						if (!isset($field['value']))
							$field['value'] = 0;
						if ($field['value'] == $field['preset'])
							$extra = ' checked="checked"';
						$field['value'] = $field['preset'];
						break;
					case 'checkbox':
						$standard_input = true;
						//$disable_value = true;
						if (!isset($field['value']))
							$field['value'] = 0;
						if ($field['value'] == $field['preset'])
							$extra = ' checked="checked"';
						$field['value'] = $field['preset'];
						break;
					case 'file':
						if (isset($field['onchange']))
							$extra = ' onchange="'.$field['onchange'].'"';
						$standard_input = true;
						break;
					case 'password':
					case 'submit':
					case 'reset':
					case 'button':
					case 'text':
						if (isset($field['placeholder']))
							$extra = ' placeholder="'.$field['placeholder'].'"';
						if (isset($field['onfocus']))
							$extra = ' onfocus="'.$field['onfocus'].'"';
						$standard_input = true;
						break;
				}

				if (!isset($field['value']))
					$field['value'] = '';

				if ($standard_input)
					$output .= '<input type="'.$field['type'].'" name="'.$field['name'].'" id="'.$field['id'].'"'.(!$disable_value ? ' value="'.$field['value'].'"' : '').(!empty($field['style']) ? ' style="'.$field['style'].'"' : '').(!empty($field['size']) ? ' size="'.$field['size'].'"' : '').(!empty($field['class']) ? ' class="'.$field['class'].'"' : '').$extra.'>';

				if (!empty($field['label']) && isset($field['append_label']) && $field['append_label'] == true)
					$output .= '<label class="prefix" for="'.$field['id'].'">'.$field['label'].'</label>';


				if (($this->errors) && ($this->inline_errors) && isset($this->errors[$field['name']]))
					$output .= '<div class="inline-error">'.$this->errors[$field['name']].'</div>';
					
				if (!empty($field['b_label']) && empty($field['append_label']))
					$output .= '<label for="'.$field['id'].'"'.(empty($field['label_class']) ? '' : ' class="'.$field['label_class'].'"').'>'.$field['b_label'].'</label>';


				if (!empty($field['append_html']))
					$output .= $field['append_html'];

				$output .= '</li>';
			}

			$output .= '</ol></fieldset>';
		}
				
		$output .= $append_last;

		$output .= '</form>';

		return $output;
	}

	function check_email_address($strEmailAddress)
	{
		// Control characters are not allowed
		if (preg_match('/[\x00-\x1F\x7F-\xFF]/', $strEmailAddress)) {
			return false;
		}

		// Check email length - min 3 (a@a), max 256
		if (!$this->check_text_length($strEmailAddress, 3, 256)) {
			return false;
		}

		// Split it into sections using last instance of "@"
		$intAtSymbol = strrpos($strEmailAddress, '@');
		if ($intAtSymbol === false) {
		// No "@" symbol in email.
			return false;
		}
		$arrEmailAddress[0] = substr($strEmailAddress, 0, $intAtSymbol);
		$arrEmailAddress[1] = substr($strEmailAddress, $intAtSymbol + 1);

		// Count the "@" symbols. Only one is allowed, except where
		// contained in quote marks in the local part. Quickest way to
		// check this is to remove anything in quotes. We also remove
		// characters escaped with backslash, and the backslash
		// character.
		$arrTempAddress[0] = preg_replace('/\./'
						,''
						,$arrEmailAddress[0]);
		$arrTempAddress[0] = preg_replace('/"[^"]+"/'
						,''
						,$arrTempAddress[0]);
		$arrTempAddress[1] = $arrEmailAddress[1];
		$strTempAddress = $arrTempAddress[0] . $arrTempAddress[1];
		// Then check - should be no "@" symbols.
		if (strrpos($strTempAddress, '@') !== false) {
		// "@" symbol found
		return false;
		}

		// Check local portion
		if (!$this->check_local_portion($arrEmailAddress[0])) {
		return false;
		}

		// Check domain portion
		if (!$this->check_domain_portion($arrEmailAddress[1])) {
		return false;
		}

		// If we're still here, all checks above passed. Email is valid.
		return true;

	}

	function check_local_portion($strLocalPortion)
	{
		// Local portion can only be from 1 to 64 characters, inclusive.
		// Please note that servers are encouraged to accept longer local
		// parts than 64 characters.
		if (!$this->check_text_length($strLocalPortion, 1, 64)) {
		return false;
		}
		// Local portion must be:
		// 1) a dot-atom (strings separated by periods)
		// 2) a quoted string
		// 3) an obsolete format string (combination of the above)
		$arrLocalPortion = explode('.', $strLocalPortion);
		for ($i = 0, $max = sizeof($arrLocalPortion); $i < $max; $i++) {
			if (!preg_match('.^('
				.    '([A-Za-z0-9!#$%&\'*+/=?^_`{|}~-]'
				.    '[A-Za-z0-9!#$%&\'*+/=?^_`{|}~-]{0,63})'
				.'|'
				.    '("[^\\\"]{0,62}")'
				.')$.'
				,$arrLocalPortion[$i])) {
			return false;
		}
		}
		return true;
	}

	function check_domain_portion($strDomainPortion)
	{
		// Total domain can only be from 1 to 255 characters, inclusive
		if (!$this->check_text_length($strDomainPortion, 1, 255)) {
		return false;
		}
		// Check if domain is IP, possibly enclosed in square brackets.
		if (preg_match('/^(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])'
		.'(\.(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])){3}$/'
		,$strDomainPortion) ||
		preg_match('/^\[(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])'
		.'(\.(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])){3}\]$/'
		,$strDomainPortion)) {
		return true;
		} else {
		$arrDomainPortion = explode('.', $strDomainPortion);
		if (sizeof($arrDomainPortion) < 2) {
			return false; // Not enough parts to domain
		}
		for ($i = 0, $max = sizeof($arrDomainPortion); $i < $max; $i++) {
			// Each portion must be between 1 and 63 characters, inclusive
			if (!$this->check_text_length($arrDomainPortion[$i], 1, 63)) {
			return false;
			}
			if (!preg_match('/^(([A-Za-z0-9][A-Za-z0-9-]{0,61}[A-Za-z0-9])|'
			.'([A-Za-z0-9]+))$/', $arrDomainPortion[$i])) {
			return false;
			}
			if ($i == $max - 1) { // TLD cannot be only numbers
			if (strlen(preg_replace('/[0-9]/', '', $arrDomainPortion[$i])) <= 0) {
				return false;
			}
			}
		}
		}
		return true;
	}

	function check_text_length($strText, $intMinimum, $intMaximum) {
		// Minimum and maximum are both inclusive
		$intTextLength = strlen($strText);
		if (($intTextLength < $intMinimum) || ($intTextLength > $intMaximum)) {
		return false;
		} else {
		return true;
		}
	}
}
