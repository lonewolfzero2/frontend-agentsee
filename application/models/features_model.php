<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Features_model extends CI_Model {
	
	
	function __construct() {
        parent::__construct();
    }
	
	// return all list of feature
	function getList(){
		return $this->db->get('features');
	}

}