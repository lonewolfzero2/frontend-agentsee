<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
	}
	
	// login page
	public function index()
	{
		$this->formgen->show_form($this->config->item('login', 'forms'));
		$this->formgen->repopulate(array('r' => $this->input->get('r')));
		$this->formgen->inline_errors = true;
		if ($this->formgen->submitted() && $this->formgen->validate())
		{
			$dataUser = $this->user_model->getUser(0,array('email' => $this->formgen->data['email']));
			if(!$dataUser){
				$this->formgen->errors['controller'] = 'That email and password combination does not match. Have another go';
			} 
			else { 
				// if email exist 
				if ($this->bcrypt->check_password($this->formgen->data['password'], $dataUser['password']))
				{
					/** password and username match **/
					if($dataUser['email_code'] == null && $dataUser['mobile_code']==null){					
						//add id into session
						$this->session->set_userdata( array(
						'id'=>$dataUser['id'],
						));
					return redirect('homepage'); // temp
					}
					else if($dataUser['email_code'] <> null && $dataUser['mobile_code']==null){
						// please verify email address
					}
					else if($dataUser['email_code'] == null && $dataUser['mobile_code']<>null){
						// please verify mobile phone
						return redirect('user/mobile_corfirmation');
					}
					else if($dataUser['email_code'] <> null && $dataUser['mobile_code']<>null){
						//please verify email and mobile phone
						/**verify mobile first then email**/
						return redirect('user/mobile_corfirmation');
					}
				 
				}
				else
				{
				  /** username match, password do not match **/
				  $this->formgen->errors['controller'] = 'That email and password combination does not match. Have another go';
				  $this->formgen->repopulate($this->formgen->data);
				}
			}
		}
		else if ($this->formgen->submitted()){
			$this->formgen->repopulate($this->formgen->data);
		}
		$data['main_view'] = 'user/login';
		$this->load->view('layout', $data);
	}
	
	/*only for agent*/
	public function registration()
	{
		$as = $this->uri->segment(3);
		if( $as != 'agent' and $as != 'buyer' ) return redirect('');
		$this->formgen->show_form($this->config->item($as, 'f_registration'));
		$this->formgen->inline_errors = true;
		if ($this->formgen->submitted() && $this->formgen->validate() && $this->formgen->check_email_address($this->formgen->data['email']) && !empty($this->formgen->data['terms']))
		{
			if($this->user_model->getUser(0,array('email' => $this->formgen->data['email']))) 
				$this->formgen->errors['controller'] = 'It looks like you have already registered. 
					Either try <a href="'.base_url().'user'.'">loging in</a>, or we can <a href="'.base_url().''.'">send you a new verification code</a>';
			else {
				$emailCode = $this->user_model->randomPrefix(16);
				$mobileCode = $this->user_model->randomMobile_code();
				$data = array(
					'type' => $as,
					'first_name' => $this->formgen->data['firstname'],
					'last_name' => $this->formgen->data['lastname'],
					'email' => $this->formgen->data['email'],
					'email_code' => $emailCode,
					'mobile' => $this->formgen->data['mobile'],
					'mobile_code' => $mobileCode,
					'password' => $this->bcrypt->hash_password($this->formgen->data['password']),
					'created' => strtotime(date("Y-m-d H:i:s")),
					'updated' => strtotime(date("Y-m-d H:i:s"))
				);
				if($as == 'agent'){
					$data['agent_licence'] = $this->formgen->data['agentlicence'];
					$data['agency_name'] = $this->formgen->data['agencyname'];
				}else if($as == 'buyer'){
					$data['buyer_type'] = $this->formgen->data['pbt'];
					$data['description'] = $this->formgen->data['description'];
				}
				$verification = $this->user_model->regUser($data); 
				
				//to do after registration complete
				/**clickatell SMS API setting**/
				$user ="";
				$pswd = "";
				$api  = "";
				$to   = $this->formgen->data['mobile'];
				$textmsg ="Agentsee verification number ".$mobileCode;
				$sms = "http://api.clickatell.com/http/sendmsg?user=".$user."&password=".$pswd."&api_id=".$api."&to=".$to."&text=".$textmsg;
				
				if($as == 'buyer'){
						//temp session for register
						$this->session->set_userdata( array(
						'forId'=>$verification,
						));
					return redirect('user/registration_step_2/buyer');
				}
				/** email **/
				$this->load->helper('url');
				$link = base_url()."user/verify/".$emailCode;
				$message = "Please verify your email address to activate your account, klik this".$link;
				$this->email->from('No Reply','Agentsee');
				$this->email->to($this->formgen->data['email']);
				$this->email->subject('Please verify your account');
				$this->email->message($message);
				$this->email->send();
				if($as == 'buyer') return redirect();
				return redirect('user/mobile_corfirmation');
			}
		}
		if ($this->formgen->submitted()){
			$this->formgen->repopulate($this->formgen->data);
		}

		$data['main_view'] = 'user/registration';
		$this->load->view('layout', $data);
	}
	// registration as casual buyer step 2
	public function registration_step_2()
	{
	if($this->session->userdata('forId')){
		$as = $this->uri->segment(3);
		if( $as != 'buyer' ) return redirect('');		
		$form = $this->config->item('registration_step_2', 'f_registration');	
			$features = $this->features_model->getList();
			$feature_to_id = array();
			$i=0;
			foreach($features->result() as $row) 
				{
				if ($i % 2 == 0)
					$liststyle ='cb';
				else
					$liststyle ='nline cb';
				$feature_to_id['feature_'.$row->id] = $row->id;
				$checkbox = array(
					'type' => 'checkbox',
					'label' => $row->name,
					'name' => 'feature_'.$row->id,
					'list_style' => $liststyle,
					'preset' => $row->id,
				);
				array_splice( $form, 17 + $i, 0, array($checkbox));
				$i++;
				}
			$this->formgen->show_form($form);
			$this->formgen->inline_errors = true;
		if ($this->formgen->submitted() && $this->formgen->validate())
		{
			// after submit
				$data = array(
					'property_type' => $this->formgen->data['type'],					
					'bedrooms' => $this->formgen->data['bedrooms'],
					'bathrooms' => $this->formgen->data['bathrooms'],
					'carplaces' => $this->formgen->data['carspots'],
					'landsize' => $this->formgen->data['landsize']
				);
				if($this->formgen->data['price_between']==0)
				{
					$data['min_price'] = $this->formgen->data['pricemin'];
					$data['max_price'] = $this->formgen->data['pricemax'];
				}
				if($this->formgen->data['price_between']==1)
				{
					$data['above_price'] = $this->formgen->data['priceabove'];
				}
				$searches_id = $this->stored_search_model->add($data,'stored_searches'); // add to table stored_searches
				$location = explode(",", $this->formgen->data['location']);
				foreach ($location as $suburb)
					{
						$this->stored_search_model->add(array(
							'suburb_id' => $suburb,
							'search_id' => $searches_id,
						),'stored_searches_suburbs');
					}
				foreach ($feature_to_id as $name => $value)
					{
					if (!empty($this->formgen->data[$name]))
						{   // add to table stored_searches_features
							$this->stored_search_model->add(array(
								'feature_id' => $value,
								'search_id' => $searches_id,
							),'stored_searches_features');
						}
					}
				
				
		}
		if ($this->formgen->submitted()){
			$this->formgen->repopulate($this->formgen->data);
		}

		$data['main_view'] = 'user/registration_step_2';
		$this->load->view('layout', $data);
	}else{
		redirect('homepage');
	}
	}
	
	public function mobile_corfirmation()
	{
		$this->formgen->show_form($this->config->item('mobile', 'forms'));
		$this->formgen->inline_errors = true;
		if ($this->formgen->submitted() && $this->formgen->validate() )
		{
			if($this->user_model->update(array('mobile_code' => $this->formgen->data['mobile']),array('mobile_code' => null)) == true){
				// redirect to somewhere
			}
			 else echo "something wrong";
		}

		$data['main_view'] = 'user/mobile';
		$this->load->view('layout', $data);
	}
		
	
	public function verify()
	{
		if($this->user_model->update( array('email_code' => $this->uri->segment(3)),array('email_code' => null))==true)
			$this->session->set_flashdata('msg', 'Thankyou, you can now login');
		else $this->session->set_flashdata('msg', 'resend email');
		$data['main_view'] = 'user/after_verify';
		$this->load->view('layout', $data);
	}
	public function trouble(){
		$this->formgen->show_form($this->config->item('trouble', 'forms'));
		$this->formgen->inline_errors = true;
		if ($this->formgen->submitted() && $this->formgen->validate() )
		{
			$dataUser = $this->user_model->getUser(0,array('email' => $this->formgen->data['email']));
			if($dataUser){
				$newPassword = $this->user_model->randomMobile_code();
				$this->user_model->update( array('email' => $this->formgen->data['email']),array('password' => $this->bcrypt->hash_password($newPassword))); 
			 /**clickatell SMS API setting**/
				$user ="";
				$pswd = "";
				$api  = "";
				$to   = $dataUser['mobile'];
				$textmsg ="Agentsee verification number ".$newPassword;
				$sms = "http://api.clickatell.com/http/sendmsg?user=".$user."&password=".$pswd."&api_id=".$api."&to=".$to."&text=".$textmsg;
			}
			else{
				$this->formgen->errors['controller'] = 'Sorry we do not have a record of that email address. <a href="">Try again</a> or <a href="">contact us</a>';
			}
		}
		if ($this->formgen->submitted()){
			$this->formgen->repopulate($this->formgen->data);
		}
		$data['main_view'] = 'user/trouble_login';
		$this->load->view('layout', $data);
	}
	
	public function logout(){
		$this->session->sess_destroy();
		return redirect();
	}
}
