<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Buyer extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
	}

	public function index(){
		$as = 'buyer';
		$this->formgen->show_form($this->config->item($as, 'f_registration'));
		$this->formgen->inline_errors = true;
		if ($this->formgen->submitted() && $this->formgen->validate() && $this->formgen->check_email_address($this->formgen->data['email']) && !empty($this->formgen->data['terms']))
		{
			if($this->user_model->getUser(0,array('email' => $this->formgen->data['email']))) 
				$this->formgen->errors['controller'] = 'It looks like you have already registered. 
					Either try <a href="'.base_url().'agent'.'">loging in</a>, or we can <a href="'.base_url().''.'">send you a new verification code</a>';
			else {
				$emailCode = $this->user_model->randomPrefix(16);
				$mobileCode = $this->user_model->randomMobile_code();
				$data = array(
					'type' => $as,
					'first_name' => $this->formgen->data['firstname'],
					'last_name' => $this->formgen->data['lastname'],
					'email' => $this->formgen->data['email'],
					'email_code' => $emailCode,
					'mobile' => $this->formgen->data['mobile'],
					'mobile_code' => $mobileCode,
					'password' => $this->bcrypt->hash_password($this->formgen->data['password']),
					'created' => strtotime(date("Y-m-d H:i:s")),
					'updated' => strtotime(date("Y-m-d H:i:s"))
				);
				if($as == 'agent'){
					$data['agent_licence'] = $this->formgen->data['agentlicence'];
					$data['agency_name'] = $this->formgen->data['agencyname'];
				}else if($as == 'buyer'){
					$data['buyer_type'] = $this->formgen->data['pbt'];
					$data['description'] = $this->formgen->data['description'];
				}
				$verification = $this->user_model->regUser($data); 
				
				//to do after registration complete
				/**clickatell SMS API setting**/
				$user ="";
				$pswd = "";
				$api  = "";
				$to   = $this->formgen->data['mobile'];
				$textmsg ="Agentsee verification number ".$mobileCode;
				$sms = "http://api.clickatell.com/http/sendmsg?user=".$user."&password=".$pswd."&api_id=".$api."&to=".$to."&text=".$textmsg;
				
				if($as == 'buyer'){
					return redirect('agent/registration_step_2/buyer');
				}
				/** email **/
				$this->load->helper('url');
				$link = base_url()."agent/verify/".$emailCode;
				$message = "Please verify your email address to activate your account, klik this".$link;
				$this->email->from('No Reply','Agentsee');
				$this->email->to($this->formgen->data['email']);
				$this->email->subject('Please verify your account');
				$this->email->message($message);
				$this->email->send();
				if($as == 'buyer') return redirect();
				return redirect('agent/mobile_corfirmation');
			}
		}
		if ($this->formgen->submitted()){
			$this->formgen->repopulate($this->formgen->data);
		}

		$data['main_view'] = 'agent/buyer';
		$this->load->view('layout', $data);
	}
	
	/**processing excel files and insert it to table buyer **/
	private function fetchFile($path,$type)
	{
		$getType= ltrim ($type,'.');
		$this->load->library('excel');
		$objReader = PHPExcel_IOFactory::createReader($this->getReader($getType));
		$objPHPExcel = $objReader->load($path); 
		$objPHPExcel->setActiveSheetIndex(0); // added for ods files bug
		$objWorksheet = $objPHPExcel->getActiveSheet(0); 
		$list = array();
		$row=1;		
		$highestColumn = $objWorksheet->getHighestColumn(); // e.g 'F'
		$columnsToRead = PHPExcel_Cell::columnIndexFromString($highestColumn);
		$header = array("lastname", "lastName", "last name", "Lastname", "last-name","firstname", "firstName", "first name", "Firstname", "first-name",
					"email", "e-mail", "email address", "Email", "Email Address", "mobile phone number", "phone", "mobile number", "mobile", "Mobile",
					"mobile phone number","Description","desc","description");
		/**Return array of non empty header**/
		for ($column = 0; $column < $columnsToRead; $column++) 
		{
			$columnTitle = $objWorksheet->getCellByColumnAndRow($column, $row)->getValue();
			if($columnTitle <> '' && in_array($columnTitle, $header)) {
				$columnName = PHPExcel_Cell::stringFromColumnIndex($column); //return column title
				$list[$column] = $columnName;
			}
		}
		$data = array();
		$lastRow=$objWorksheet->getHighestDataRow();
		for($row = 2; $row <= $lastRow; $row++ )
		{
			foreach($list as $dbColumn)
			{
				 $data[$row][$dbColumn] = $objWorksheet->getCell($dbColumn.$row)->getValue();
			}
		}
		return $data;
	}
	
	/**return the reader for various spreadsheet extension **/
	private function getReader($type)
	{
		switch ($type) 
		{
			case 'xlsx': return 'Excel2007'; break;
			case 'xls': return 'Excel5'; break;
			case 'ods': return 'OOCalc'; break;
			case 'slk': return 'SYLK'; break;
			case 'xml': return 'Excel2003XML'; break;
			case 'csv': return 'csv'; break;
		}
	}
}
