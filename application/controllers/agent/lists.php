<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Lists extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
	}
	public function index(){

	}
	
	public function add(){
		$datas = $this->user_model->getUser($this->session->userdata('id'),array());
		if($this->session->userdata('id') && isset($datas['type']) )
			{
			$data['type']=$datas['type'];
			$form = $this->config->item('addlisting', 'f_listing');	
			$features = $this->features_model->getList();
			$feature_to_id = array();
			$i=0;
			foreach($features->result() as $row) 
				{
				if ($i % 2 == 0)
					$liststyle ='cb';
				else
					$liststyle ='nline cb';
				$feature_to_id['feature_'.$row->id] = $row->id;
				$checkbox = array(
					'type' => 'checkbox',
					'label' => $row->name,
					'name' => 'feature_'.$row->id,
					'list_style' => $liststyle,
					'preset' => $row->id,
				);
				array_splice( $form, 24 + $i, 0, array($checkbox));
				$i++;
				}
			$this->formgen->show_form($form);
			$this->formgen->inline_errors = true;
			if ($this->formgen->submitted() && $this->formgen->validate())
				{ 
				$address = explode(",", $this->formgen->data['location']);
				$data = array(
					'suburb_id' => $this->formgen->data['id_suburb'],
					'address_unit' => $address[2],
					'address_number' =>$address[0],					
					'address_street' => $address[1],
					'property_type' => $this->formgen->data['type'],					
					'bedrooms' => $this->formgen->data['bedrooms'],
					'bathrooms' => $this->formgen->data['bathrooms'],
					'carplaces' => $this->formgen->data['carspots'],
					'landsize' => $this->formgen->data['landsize'],
					'created' => strtotime(date("Y-m-d H:i:s")),
					'updated' => strtotime(date("Y-m-d H:i:s"))
				);
				if($this->formgen->data['price_between']==0)
				{
					$data['min_price'] = $this->formgen->data['pricemin'];
					$data['max_price'] = $this->formgen->data['pricemax'];
				}
				if($this->formgen->data['price_between']==1)
				{
					$data['above_price'] = $this->formgen->data['priceabove'];
				}
				$property_id = $this->properties_model->add($data);
				foreach ($feature_to_id as $name => $value)
					{
					if (!empty($this->formgen->data[$name]))
						{
						$this->properties_model->addPropertyFeature(array(
							'feature_id' => $value,
							'property_id' => $property_id,
						));
						}
					}
				// handling file

				}
			if ($this->formgen->submitted())
				{
				$this->formgen->repopulate($this->formgen->data);
				}
			$data['main_view'] = 'agent/add_listing';
			}
		else
			{
			return redirect('homepage');
			}		
		$this->load->view('layout', $data);				
	}
	
}
