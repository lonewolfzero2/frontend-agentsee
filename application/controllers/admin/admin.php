<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->library(array('session'));
	}

	public function index(){
	
		$this->formgen->show_form($this->config->item('login', 'forms'));
		$this->formgen->inline_errors = true;
		if ($this->formgen->submitted() && $this->formgen->validate())
		{
			$dataUser = $this->user_model->getUser(0,array('email' => $this->formgen->data['email']));
			if(!$dataUser){
				$this->formgen->errors['controller'] = 'That email and password combination does not match. Have another go';
			} 
			else { 
				// if email exist 
				if ($this->bcrypt->check_password($this->formgen->data['password'], $dataUser['password']) && $dataUser['type']=='Admin')
				{			
						//add id into session
						$this->session->set_userdata( array(
						'id'=>$dataUser['id'],
						));
					
					return redirect('admin/dashboard'); // redirect dashboard admin
				}
				else
				{
				  /** username match, password do not match **/
				  $this->formgen->errors['controller'] = 'That email and password combination does not match. Have another go';
				  $this->formgen->repopulate($this->formgen->data);
				}
			}
		
		} else if ($this->formgen->submitted())
		{
			$this->formgen->repopulate($this->formgen->data);
		}
		$datas = $this->user_model->getUser($this->session->userdata('id'),array());
		if($this->session->userdata('id') && ($datas['type'] == 'Admin') ){
			return redirect('admin/dashboard'); // redirect dashboar admin
		}else{
			$data['main_view'] = 'admin/login';
		}
		$this->load->view('admin/layout', $data);
	}
	public function logout(){
		$this->session->sess_destroy();
		return redirect('admin');
	}

}
