<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Homepage extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
	}
	public function index(){
		if($this->session->userdata('id'))$session = $this->session->userdata('id');
		else $session = 0;
		$datas = $this->user_model->getUser($session,array());
		if($this->session->userdata('id') && isset($datas['type']) ){
			$data['type']=$datas['type'];
			$data['main_view'] = 'agent/homepage';
			$data['header'] = 'agent/header';
		}
		else $data['main_view'] = 'homepage/index';
		$this->load->view('layout', $data);
	}
}
