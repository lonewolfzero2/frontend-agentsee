<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
if(!isset($main_view)){ die(); }
if(!isset($header))$this -> load -> view('header');
else $this -> load -> view($header); 
$this -> load -> view('menu'); 
$this -> load -> view($main_view); 
$this -> load -> view('footer'); 

