<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


$autoload['packages'] = array(APPPATH.'third_party');
$autoload['libraries'] = array('database','session','form_validation','pagination','image_lib','user_agent','parser','email','upload','FormGen','Bcrypt');
$autoload['helper'] = array('url','form','text','date','html','file','inflector','download','directory','xml','security');
$autoload['model']=array('user_model','suburbs_model','features_model','buyer_model','properties_model');
$autoload['config'] = array('forms','f_registration','f_listing','f_suburb','f_buyer');