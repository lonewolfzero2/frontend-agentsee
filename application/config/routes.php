<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$route['default_controller'] = "homepage";
$route['404_override'] = '';
$route['script.js'] = 'assets/js/0'; 
$route['style.css'] = 'assets/css/0';
$route['test1/(:num)'] = "test1/index/$1";