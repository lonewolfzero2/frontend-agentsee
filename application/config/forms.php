<?php
$config['forms'] = array(
	'confirm' => array(
		array(
			'type' => 'submit',
			'name' => 'submit',
			'id' => 'submit1',
			'value' => 'Yes',
		),
		array(
			'type' => 'submit',
			'name' => 'submit',
			'id' => 'submit2',
			'value' => 'No',
		),
	),
	'login' => array(
		array(
			'type' => 'hidden',
			'name' => 'r',
			'value' => '',
			),
		array(
			'type' => 'text',
			'label' => 'Email',
			'name' => 'email',
			'validation' => array('required'),
			),
		array(
			'type' => 'password',
			'label' => 'Password',
			'name' => 'password',
			'validation' => array('trim','required'),
			),
		array(
			'type' => 'submit',
			'name' => 'submit',
			'value' => 'Log in',
			'list_style' => 'center_button',
			),
		),
	'mobile' => array(
		array(
			'type' => 'hidden',
			'name' => 'r',
			'value' => '',
			),
		array(
			'type' => 'text',
			'label' => 'Mobile',
			'name' => 'mobile',
			'validation' => array('required'),
			),
		array(
			'type' => 'submit',
			'name' => 'submit',
			'value' => 'Verify',
			'list_style' => 'center_button',
			),
		),
	'trouble' => array(
		array(
			'type' => 'hidden',
			'name' => 'r',
			'value' => '',
			),
		array(
			'type' => 'text',
			'label' => 'my email address',
			'name' => 'email',
			'validation' => array('trim', 'valid_email', 'required'),
			),
		array(
			'type' => 'submit',
			'name' => 'submit',
			'value' => 'Send new code',
			'list_style' => 'center_button',
			),
		),
	);
