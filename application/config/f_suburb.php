<?php	
$config['f_suburb'] = array(
	'add' => array(
		array(
			'type' => 'text',
			'label' => 'Address number',
			'name' => 'number',
			),
		array(
			'type' => 'text',
			'label' => 'Address street',
			'name' => 'street',
			),
		array(
			'type' => 'text',
			'label' => 'Address unit',
			'name' => 'unit',
			),
		array(
			'type' => 'text',
			'label' => 'Suburb',
			'name' => 'suburb',
			'validation' => array('trim','required'),
			),
		array(
			'type' => 'text',
			'label' => 'State',
			'name' => 'state',
			'validation' => array('trim','required'),
			),		
		array(
			'type' => 'text',
			'label' => 'Postal code',
			'name' => 'postcode',
			'validation' => array('trim','required','is_numeric'),
			),
		array(
			'type' => 'submit',
			'name' => 'submit',
			'value' => 'Save',
			),
		),
	'edit' => array(
		array(
			'type' => 'text',
			'label' => 'Suburb',
			'name' => 'e_suburb',
			'validation' => array('required'),
			),
		array(
			'type' => 'text',
			'label' => 'Postalcode',
			'name' => 'e_postcode',
			'validation' => array('trim','required',''),
			),	
		array(
			'type' => 'text',
			'label' => 'State',
			'name' => 'e_state',
			'validation' => array('trim','required'),
			),
		array(
			'type' => 'submit',
			'name' => 'submit',
			'value' => 'Save',
			'list_style' => 'center_button',
			),
		),
		);
?>